<?php
interface ManyToOneRelatable {
	/**
	 * @return ManyToOneRelation[]
	 */
	public function getManyToOneRelations();
}

class ManyToOneRelation {
	private $attribute;
	private $foreignKey;
	private $relatedClass;
	
	public function __construct($attribute, $foreignKey, $relatedClass) {
		$this->attribute = $attribute;
		$this->foreignKey = $foreignKey;
		$this->relatedClass = $relatedClass;
	}
	
	public function getAttribute() {
		return $this->attribute;
	}
	
	public function getForeignKey() {
		return $this->foreignKey;
	}
	
	public function getRelatedClass() {
		return $this->relatedClass;
	}
}