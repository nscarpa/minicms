<?php
include_once dirname ( __FILE__ ) . '/Driver.php';
class Repository {
	protected $driver;
	public function __construct(Driver $driver) {
		$this->driver = $driver;
	}
	protected function bind($row, $class) {
		$rC = new ReflectionClass ( $class );
		$object = $rC->newInstanceArgs ( array (
				'id' => $row ['id'] 
		) );
		// Rimuove perch� passato nel costruttore
		unset ( $row ['id'] );
		// Convenzione implicita sui nomi per binding colonne - attributi
		foreach ( $row as $key => $value ) {
			$methodName = 'set' . ucfirst ( $key );
			if ($rC->hasMethod ( $methodName )) {
				$rM = $rC->getMethod ( $methodName );
				$rM->invoke ( $object, $value );
			}
		}
		
		if (is_a ( $object, 'ManyToOneRelatable' )) {
			$relations = $object->getManyToOneRelations ();
			foreach ( $relations as $relation ) {
				$foreignKey = $relation->getForeignKey ();
				$foreignId = $row [$foreignKey];
				$repositoryClass = $relation->getRelatedClass () . 'Repository';
				$rRC = new ReflectionClass ( $repositoryClass );
				$repository = $rRC->newInstanceArgs ( array (
						'driver' => $this->driver 
				) );
				$relatedObject = $repository->getById ( $foreignId );
				$rM = $rC->getMethod ( 'set' . ucfirst ( $relation->getAttribute () ) );
				$rM->invoke ( $object, $relatedObject );
			}
		}
		
		if (is_a ( $object, 'ManyToManyRelatable' )) {
			$relations = $object->getManyToManyRelations ();
			foreach ( $relations as $relation ) {
				$table = $relation->getTable ();
				$key = $relation->getKey ();
				$foreignKey = $relation->getForeignKey ();
				// TODO: Creare repository per relazioni
				$data = $this->driver->query ( "SELECT $foreignKey FROM $table WHERE $key = {$object->getId()}" );
				// TODO: Riformattazione dell'array, esporre metodo prepare in Repository
				$foreignIds = array();
				foreach ($data as $key => $value) {
					$foreignIds[] = $value[$foreignKey];
				}
				$repositoryClass = $relation->getRelatedClass () . 'Repository';
				
				$rRC = new ReflectionClass ( $repositoryClass );
				$repository = $rRC->newInstanceArgs ( array (
						'driver' => $this->driver 
				) );
				$relatedObjects = $repository->getByIds ( $foreignIds );
				$rM = $rC->getMethod ( 'set' . ucfirst ( $relation->getAttribute () ) );
				$rM->invoke ( $object, $relatedObjects );
			}
		}
		
		return $object;
	}
	protected function bindArray($data, $class) {
		$objects = array ();
		foreach ( $data as $row ) {
			$objects [] = $this->bind ( $row, $class );
		}
		return $objects;
	}
}