<?php
interface ManyToManyRelatable {
	public function getManyToManyRelations();
}

class ManyToManyRelation {
	private $attribute;
	private $table;
	private $key;
	private $foreignKey;
	private $relatedClass;
	
	public function __construct($attribute, $table, $key, $foreignKey, $relatedClass) {
		$this->attribute = $attribute;
		$this->table = $table;
		$this->key = $key;
		$this->foreignKey = $foreignKey;
		$this->relatedClass = $relatedClass;
	}
	public function getAttribute() {
		return $this->attribute;
	}
	public function getTable() {
		return $this->table;
	}
	public function getKey() {
		return $this->key;
	}
	public function getForeignKey() {
		return $this->foreignKey;
	}
	public function getRelatedClass() {
		return $this->relatedClass;
	}
}