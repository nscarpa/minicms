<?php
class Driver {
	private static $instance = null;
	private $driver;
	
	private function __construct() {
		$dbPath = dirname(__FILE__).'/../db/mini_cms.sqlite3';
		// Create (connect to) SQLite database in file
		$driver = new PDO ( 'sqlite:'.$dbPath );
		// Set errormode to exceptions
		$driver->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		
		$this->driver = $driver;
	}
	
	public static function getInstance() {
		if (self::$instance == null) {
			self::$instance = new Driver();
		}
		
		return self::$instance;
	}
	
	public function query($query) {
		$statement = $this->driver->query($query);
		return $statement->fetchAll(PDO::FETCH_ASSOC);
	}
	
	/**
	 * TODO: Refactor (PostManager)
	 * @return PDO
	 */
	public function getDBAL() {
		return $this->driver;
	}
}