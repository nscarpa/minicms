<?php
class Controller {
	function render($viewFileName, $viewParams = null) {
		$title = 'Default';
		
		if (!is_null($viewParams)) {
			extract ( $viewParams );
		}
		
		ob_start();		
		$viewFilePath = dirname ( __FILE__ ) . '/../src/view/' . $viewFileName;
		include $viewFilePath;
		$content = ob_get_clean();
		
		include dirname ( __FILE__ ) . '/../src/view/layout.html.php';
	}
}