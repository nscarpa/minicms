<?php
class Router {
	/**
	 * URL: controller
	 * action
	 */
	public static function dispatch() {
		$controllerClass = self::getControllerClass ();
		$actionMethod = self::getActionMethod ();
		
		// load class
		$controllerClassFileName = $controllerClass . '.php';
		$controllerClassFilePath = dirname ( __FILE__ ) . '/../src/controller/' . $controllerClassFileName;
		include ($controllerClassFilePath);
		
		// trigger method
		$controller = new $controllerClass ();
		$controller->$actionMethod ();
	}
	public static function getControllerClass() {
		$controller_name = 'Default';
		if (isset ( $_GET ['controller'] ) && ! empty ( $_GET ['controller'] )) {
			$tokens = explode ( '_', $_GET ['controller'] );
			array_walk ( $tokens, function (&$token) {
				$token = ucfirst ( $token );
			} );
			$controller_name = implode ( '', $tokens );
		}
		return $controller_name . 'Controller';
	}
	public static function getActionMethod() {
		$action_name = 'index';
		if (isset ( $_GET ['action'] ) && ! empty ( $_GET ['action'] )) {
			$action_name = $_GET ['action'];
		}
		return $action_name . 'Action';
	}
}