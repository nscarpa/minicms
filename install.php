<?php
try {
	/**
	 * ************************************
	 * Create databases and *
	 * open connections *
	 * ************************************
	 */
	
	// Create (connect to) SQLite database in file
	$file_db = new PDO ( 'sqlite:db/mini_cms.sqlite3' );
	// Set errormode to exceptions
	$file_db->setAttribute ( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	
	/**
	 * ************************************
	 * Create tables *
	 * ************************************
	 */
	
	// Drop table messages from file db
	$file_db->exec("DROP TABLE tag");
	$file_db->exec("DROP TABLE categoria");
	$file_db->exec("DROP TABLE pagina_statica");
	$file_db->exec("DROP TABLE post");
	$file_db->exec("DROP TABLE post_tag");
	echo "Old tables deleted successfully<br/>";
	
	// Create table tag
	$file_db->exec ( "CREATE TABLE IF NOT EXISTS tag(
					id integer PRIMARY KEY, 
					nome text UNIQUE NOT NULL)" );
	
	echo "Table tag created successfully<br/>";
	
	// Create table categoria
	$file_db->exec ( "CREATE TABLE IF NOT EXISTS categoria(
					id integer PRIMARY KEY, 
					nome text UNIQUE NOT NULL)" );
	
	echo "Table categoria created successfully<br/>";
	
	// Create table pagina_statica
	$file_db->exec ( "CREATE TABLE IF NOT EXISTS pagina_statica(
					id integer PRIMARY KEY, 
					titolo text UNIQUE NOT NULL, 
					corpo text NOT NULL)" );
	
	echo "Table pagina_statica created successfully<br/>";
	
	// Create table pagina_statica
	$file_db->exec ( "CREATE TABLE IF NOT EXISTS post(
					id integer PRIMARY KEY, 
					titolo text UNIQUE NOT NULL, 
					corpo text NOT NULL, 
					categoria_id integer)" );
	
	echo "Table pagina_statica created successfully<br/>";
	
	// Create table pagina_statica
	$file_db->exec ( "CREATE TABLE IF NOT EXISTS post_tag(
					post_id integer NOT NULL, 
					tag_id integer NOT NULL, 
					PRIMARY KEY(post_id, tag_id))" );
	
	echo "Table post_tag created successfully<br/>";
	
	/**
	 * ************************************
	 * Set initial data *
	 * ************************************
	 */
	
	// Array with some test data to insert to database
	$tags = array (
			array (
					'nome' => 'Tag 1' 
			),
			array (
					'nome' => 'Tag 2' 
			),
			array (
					'nome' => 'Tag 3' 
			) 
	);
	
	/**
	 * ************************************
	 * Play with databases and tables *
	 * ************************************
	 */
	
	// Prepare INSERT statement to SQLite3 file db
	$insert = "INSERT INTO tag (nome)
                VALUES (:nome)";
	$stmt = $file_db->prepare ( $insert );
	
	// Bind parameters to statement variables
	$stmt->bindParam ( ':nome', $nome );
	
	// Loop thru all messages and execute prepared insert statement
	foreach ( $tags as $tag ) {
		$nome = $tag ['nome'];
		
		// Execute statement
		$stmt->execute ();
	}
	echo "Fixture tabella tag inserite correttamente<br/>";
	
	/**
	 * ************************************
	 * Set initial data *
	 * ************************************
	 */
	
	// Array with some test data to insert to database
	$categorie = array (
			array (
					'nome' => 'Categoria 1'
			),
			array (
					'nome' => 'Categoria 2'
			),
			array (
					'nome' => 'Categoria 3'
			)
	);
	
	/**
	 * ************************************
	 * Play with databases and tables *
	 * ************************************
	*/
	
	// Prepare INSERT statement to SQLite3 file db
	$insert = "INSERT INTO categoria (nome)
                VALUES (:nome)";
	$stmt = $file_db->prepare ( $insert );
	
	// Bind parameters to statement variables
	$stmt->bindParam ( ':nome', $nome );
	
	// Loop thru all messages and execute prepared insert statement
	foreach ( $categorie as $categoria ) {
		$nome = $categoria ['nome'];
	
		// Execute statement
		$stmt->execute ();
	}
	echo "Fixture tabella categoria inserite correttamente<br/>";
	
	/**
	 * ************************************
	 * Set initial data *
	 * ************************************
	 */
	
	// Array with some test data to insert to database
	$pagine_statiche = array (
			array (
					'titolo' => 'About',
					'corpo' => '<div><strong>About ...</strong></div>'
			),
			array (
					'titolo' => 'Contatti',
					'corpo'	=> '<div><p><a href="mailto:nicolo.scarpa@gmail.com">nicolo.scarpa@gmail.com</a></p></div>'
			)
	);
	
	/**
	 * ************************************
	 * Play with databases and tables *
	 * ************************************
	*/
	
	// Prepare INSERT statement to SQLite3 file db
	$insert = "INSERT INTO pagina_statica (titolo, corpo)
                VALUES (:titolo, :corpo)";
	$stmt = $file_db->prepare ( $insert );
	
	// Bind parameters to statement variables
	$stmt->bindParam ( ':titolo', $titolo );
	$stmt->bindParam ( ':corpo', $corpo );
	
	// Loop thru all messages and execute prepared insert statement
	foreach ( $pagine_statiche as $pagina_statica ) {
		$titolo = $pagina_statica ['titolo'];
		$corpo = $pagina_statica ['corpo'];
	
		// Execute statement
		$stmt->execute ();
	}
	
	echo 'Fixture tabella pagina_statica inserite correttamente</br>';
	
	/**
	 * ************************************
	 * Close db connections *
	 * ************************************
	 */
	
	// Close file db connection
	$file_db = null;
} catch ( PDOException $e ) {
	// Print PDOException message
	echo $e->getMessage ();
}
?>