<?php
include_once dirname ( __FILE__ ) . '/../../lib/ManyToOneRelatable.php';
include_once dirname ( __FILE__ ) . '/../../lib/ManyToManyRelatable.php';
class Post implements ManyToOneRelatable, ManyToManyRelatable {
	private $id;
	private $titolo;
	private $corpo;
	private $categoria;
	private $tags;
	
	/**
	 *
	 * @param int $id        	
	 */
	public function __construct($id = null) {
		if (! is_null ( $id )) {
			$this->id = $id;
		}
	}
	
	/**
	 *
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 *
	 * @return string
	 */
	public function getTitolo() {
		return $this->titolo;
	}
	
	/**
	 *
	 * @param string $titolo        	
	 */
	public function setTitolo($titolo) {
		$this->titolo = $titolo;
		return $this;
	}
	
	/**
	 *
	 * @return string
	 */
	public function getCorpo() {
		return $this->corpo;
	}
	
	/**
	 *
	 * @param string $corpo        	
	 */
	public function setCorpo($corpo) {
		$this->corpo = $corpo;
	}
	
	/**
	 *
	 * @return Categoria
	 */
	public function getCategoria() {
		return $this->categoria;
	}
	
	/**
	 *
	 * @param Categoria $categoria        	
	 */
	public function setCategoria($categoria) {
		$this->categoria = $categoria;
	}
	
	/**
	 *
	 * @return Tag[]
	 */
	public function getTags() {
		return $this->tags;
	}
	
	/**
	 *
	 * @param Tag[] $tags        	
	 */
	public function setTags($tags) {
		$this->tags = $tags;
	}
	
	/**
	 * (non-PHPdoc)
	 * 
	 * @see ManyToOneRelatable::getManyToOneRelations()
	 */
	public function getManyToOneRelations() {
		$manyToOneRelation = new ManyToOneRelation ( 'categoria', 'categoria_id', 'Categoria' );
		return array (
				$manyToOneRelation 
		);
	}
	
	/**
	 *
	 * @return ManyToManyRelation[]
	 */
	public function getManyToManyRelations() {
		$manyToManyRelation = new ManyToManyRelation ( 'tags', 'post_tag', 'post_id', 'tag_id', 'Tag' );
		return array (
				$manyToManyRelation 
		);
	}
}
