<?php
class PaginaStatica {
	private $id;
	private $titolo;
	private $corpo;
	
	/**
	 *
	 * @param int $id        	
	 */
	public function __construct($id = null) {
		if (! is_null ( $id )) {
			$this->id = $id;
		}
	}
	
	/**
	 *
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 *
	 * @return string
	 */
	public function getTitolo() {
		return $this->titolo;
	}
	
	/**
	 *
	 * @param string $titolo        	
	 */
	public function setTitolo($titolo) {
		$this->titolo = $titolo;
		return $this;
	}
	
	/**
	 *
	 * @return string
	 */
	public function getCorpo() {
		return $this->corpo;
	}
	
	/**
	 *
	 * @param string $corpo        	
	 */
	public function setCorpo($corpo) {
		$this->corpo = $corpo;
	}
}