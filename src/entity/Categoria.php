<?php
class Categoria {
	private $id;
	private $nome;
	private $posts;
	
	/**
	 *
	 * @param int $id        	
	 */
	public function __construct($id = null) {
		if (! is_null ( $id )) {
			$this->id = $id;
		}
	}
	
	/**
	 *
	 * @return int
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 *
	 * @return string
	 */
	public function getNome() {
		return $this->nome;
	}
	
	/**
	 *
	 * @param string $nome        	
	 */
	public function setNome($nome) {
		$this->nome = $nome;
	}
	
	/**
	 *
	 * @return Post[]
	 */
	public function getPosts() {
		return $this->posts;
	}
	
	/**
	 *
	 * @param Post[] $posts        	
	 */
	public function setPosts($posts) {
		$this->posts = $posts;
	}
}