<?php
class PaginaStaticaManager {
	private $driver;
	public function __construct(PDO $driver) {
		$this->driver = $driver;
	}
	public function persist(PaginaStatica $paginaStatica) {
		$is_update = false;
		if ($paginaStatica->getId () !== null) {
			$is_update = true;
		}
		
		$query = "INSERT INTO pagina_statica 
					(titolo, corpo)
                	VALUES (:titolo, :corpo)";
		if ($is_update) {
			$query = "UPDATE pagina_statica
						SET titolo=:titolo, corpo=:corpo
						WHERE id = :id";
		}
		
		$stmt = $this->driver->prepare ( $query );
		
		if ($is_update) {
			$id = $paginaStatica->getId ();
			$stmt->bindParam ( ':id', $id );
		}
		
		$titolo = $paginaStatica->getTitolo ();
		$corpo = $paginaStatica->getCorpo ();
		$stmt->bindParam ( ':titolo', $titolo );
		$stmt->bindParam ( ':corpo', $corpo );
		
		$stmt->execute ();
	}
}