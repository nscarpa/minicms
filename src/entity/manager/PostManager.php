<?php
class PostManager {
	private $driver;
	public function __construct(PDO $driver) {
		$this->driver = $driver;
	}
	public function persist(Post $post) {
		if ($post->getId () !== null) {
			$this->update ( $post );
		} else {
			$this->insert ( $post );
		}
	}
	private function update(Post $post) {
		$update = "UPDATE post 
				SET titolo=:titolo, corpo=:corpo, categoria_id=:categoria_id 
				WHERE id = :id";
		$stmt = $this->driver->prepare ( $update );
		$id = $post->getId ();
		$titolo = $post->getTitolo ();
		$corpo = $post->getCorpo ();
		$categoria_id = $post->getCategoria ()->getId ();
		$stmt->bindParam ( ':titolo', $titolo );
		$stmt->bindParam ( ':corpo', $corpo );
		$stmt->bindParam ( ':categoria_id', $categoria_id );
		$stmt->bindParam ( ':id', $id );
		$stmt->execute ();
		
		// TODO: responsabilitą non pertinente
		$delete = "DELETE FROM post_tag WHERE post_id = :post_id";
		$stmt = $this->driver->prepare ( $delete );
		$stmt->bindParam ( 'post_id', $id );
		$stmt->execute ();
		
		// TODO: responsabilitą non pertinente
		$insert = "INSERT INTO post_tag (post_id, tag_id)
				VALUES (:post_id, :tag_id)";
		$stmt = $this->driver->prepare ( $insert );
		$stmt->bindParam ( ':post_id', $id );
		$stmt->bindParam ( ':tag_id', $tag_id );
		foreach ( $post->getTags () as $tag ) {
			$tag_id = $tag->getId ();
			$stmt->execute ();
		}
	}
	private function insert(Post $post) {
		$insert = "INSERT INTO post (titolo, corpo, categoria_id)
                VALUES (:titolo, :corpo, :categoria_id)";
		$stmt = $this->driver->prepare ( $insert );
		$id = $post->getId ();
		$titolo = $post->getTitolo ();
		$corpo = $post->getCorpo ();
		$categoria_id = $post->getCategoria()->getId();
		$stmt->bindParam ( ':titolo', $titolo );
		$stmt->bindParam ( ':corpo', $corpo );
		$stmt->bindParam ( ':categoria_id', $categoria_id );
		$stmt->execute ();
		
		// TODO: responsabilitą non pertinente
		$insert = "INSERT INTO post_tag (post_id, tag_id)
				VALUES (:post_id, :tag_id)";
		$stmt = $this->driver->prepare ( $insert );
		$stmt->bindParam ( ':post_id', $post_id );
		$stmt->bindParam ( ':tag_id', $tag_id );
		$post_id = $this->driver->lastInsertId ();
		foreach ( $post->getTags () as $tag ) {
			$tag_id = $tag->getId ();
			$stmt->execute ();
		}
	}
}