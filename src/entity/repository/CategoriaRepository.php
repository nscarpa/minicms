<?php
include_once dirname ( __FILE__ ) . '/../Categoria.php';
include_once dirname ( __FILE__ ) . '/../../../lib/Repository.php';
class CategoriaRepository extends Repository {
	/**
	 * 
	 * @return Categoria[]
	 */
	public function getAll() {
		$data = $this->driver->query ( "SELECT * FROM categoria" );
		return $this->bindArray ( $data, 'Categoria' );
	}
	/**
	 * 
	 * @param int $id
	 * @return Categoria
	 */
	public function getById($id) {
		$data = $this->driver->query ( "SELECT * FROM categoria WHERE id = $id" );
		
		$row = $data [0];
		
		return $this->bind ( $row, 'Categoria' );
	}
}