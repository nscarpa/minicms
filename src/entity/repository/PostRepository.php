<?php
include_once dirname ( __FILE__ ) . '/../Post.php';
include_once dirname ( __FILE__ ) . '/CategoriaRepository.php';
include_once dirname ( __FILE__ ) . '/../../../lib/Repository.php';
class PostRepository extends Repository {
	/**
	 * 
	 * @return Post[]
	 */
	public function getAll() {
		$data = $this->driver->query ( "SELECT * FROM post" );
		
		return $this->bindArray ( $data, 'Post' );
	}
	/**
	 * 
	 * @param int $id
	 * @return Post
	 */
	public function getById($id) {
		$data = $this->driver->query ( "SELECT * FROM post WHERE id = $id" );
		
		// TODO: migliorare driver (ora ritorna solo array di righe)
		$row = $data [0];
		
		return $this->bind ( $row, 'Post' );
	}
}