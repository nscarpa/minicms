<?php
include_once dirname ( __FILE__ ) . '/../Tag.php';
include_once dirname ( __FILE__ ) . '/../../../lib/Repository.php';
include_once dirname ( __FILE__ ) . '/../../../lib/Repository.php';
class TagRepository extends Repository {
	/**
	 * 
	 * @return Tag[]
	 */
	public function getAll() {
		$data = $this->driver->query("SELECT * FROM tag");
				
		return $this->bindArray($data, 'Tag');
	}
	/**
	 * 
	 * @param int $id
	 * @return Tag
	 */
	public function getById($id) {
		$data = $this->driver->query("SELECT * FROM tag WHERE id = $id");
		
		$row = $data[0];
		
		return $this->bind($row, 'Tag');
	}
	/**
	 *
	 * @return Tag[]
	 */
	public function getByIds($ids) {
		$tags = array ();
		foreach ( $ids as $id ) {
			$tags [] = $this->getById ( $id );
		}
		return $tags;
	}
}