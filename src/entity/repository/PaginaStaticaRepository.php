<?php
include_once dirname ( __FILE__ ) . '/../PaginaStatica.php';
include_once dirname ( __FILE__ ) . '/../../../lib/Repository.php';
class PaginaStaticaRepository extends Repository {
	/**
	 * 
	 * @return PaginaStatica[]
	 */
	public function getAll() {
		$data = $this->driver->query ( "SELECT * FROM pagina_statica" );
		
		return $this->bindArray($data, 'PaginaStatica');
	}
	
	/**
	 * 
	 * @param int $id
	 * @return PaginaStatica
	 */
	public function getById($id) {
		$data = $this->driver->query("SELECT * FROM pagina_statica WHERE id = $id");
		
		$row = $data[0];
		
		return $this->bind($row, 'PaginaStatica');
	}
}