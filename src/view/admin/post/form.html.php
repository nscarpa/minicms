<h1><?php echo ( $post->getId() ? 'Modifica' : 'Nuovo' );?> Post</h1>
<script type="text/javascript" src="script/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea"
 });
</script>
<form action="index.php?controller=post&action=post" method="post">
	<input type="hidden" name="id" value="<?php echo $post->getId(); ?>" />
	<label for="titolo_text">Titolo</label> <input id="titolo_text"
		type="text" name="titolo" value="<?php echo $post->getTitolo(); ?>"
		required="required" /> <label for="corpo_textarea">Corpo</label>
	<textarea id="corpo_textarea" name="corpo" novalidate><?php echo $post->getCorpo(); ?></textarea>
	<label for="tags_select">Tags</label> <select id="tags_select"
		multiple="multiple" name="tags_ids[]">
	  <?php foreach ( $tags as $tag ) {?>
	  <option value="<?php echo $tag->getId();?>"
	  		<?php if (!empty($post->getTags())) {?>
				<?php foreach($post->getTags() as $post_tag){ ?> 
					<?php if ($post_tag->getId() == $tag->getId()) {?>
					selected="selected" 
					<?php } ?>
				<?php } ?>
			<?php } ?>>
			<?php echo $tag->getNome(); ?>
	  </option>
	  <?php } ?>
	</select> <label for="categoria_select">Categoria</label> <select
		id="categoria_select" name="categoria_id" required="required">
		<option value="">Scegli una categoria</option>
		<?php foreach ($categorie as $categoria) {?>
		<option value="<?php echo $categoria->getId();?>"
			<?php if(!empty($post->getCategoria())){?>
				<?php echo ($post->getCategoria()->getId() == $categoria->getId() ? 'selected="selected"': ''); ?>
			<?php }?>>
			<?php echo $categoria->getNome(); ?></option>
		<?php }?>
	</select> <input type="submit" name="submit" value="Submit" />
</form>