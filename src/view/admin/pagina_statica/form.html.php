<h1><?php echo ( $pagina_statica->getId() ? 'Modifica' : 'Nuova' );?> Pagina Statica</h1>
<script type="text/javascript" src="script/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea"
 });
</script>
<form action="index.php?controller=pagina_statica&action=post" method="post">
	<input type="hidden" name="id" value="<?php echo $pagina_statica->getId(); ?>" />
	<label for="titolo_text">Titolo</label> <input id="titolo_text"
		type="text" name="titolo" value="<?php echo $pagina_statica->getTitolo(); ?>"
		required="required" /> <label for="corpo_textarea">Corpo</label>
	<textarea id="corpo_textarea" name="corpo" novalidate><?php echo $pagina_statica->getCorpo(); ?></textarea>
	<input type="submit" name="submit" value="Submit" />
</form>