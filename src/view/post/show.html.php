<script type="text/javascript" src="script/jquery.sharrre.min.js"></script>
<div>
	<h1><?php echo $post->getTitolo(); ?></h1>
	<div>
		Categoria: <?php echo $post->getCategoria()->getNome();?>
	</div>
	<div>
		Tags:
		<ul>
		<?php foreach ($post->getTags() as $tag) {?>
		<li><?php echo $tag->getNome();?></li>
		<?php }?>
		</ul>
	</div>
	<div>
		<?php echo $post->getCorpo();?>
	</div>
	<div id="sharePost"
		data-url="http://localhost/miniCMS/web/index.php?controller=post&action=show&id=<?php echo $post->getId(); ?>"
		data-text="Condivisione post miniCMS: <?php echo $post->getTitolo(); ?>"
		data-title="Share"></div>
	<script type="text/javascript">
	$('#sharePost').sharrre({
		  share: {
		    googlePlus: true,
		    facebook: true,
		    twitter: true
		  },
		  buttons: {
		    googlePlus: {size: 'tall', annotation:'bubble'},
		    facebook: {layout: 'box_count'},
		    twitter: {count: 'vertical', via: '_JulienH'}
		  },
		  hover: function(api, options){
		    $(api.element).find('.buttons').show();
		  },
		  hide: function(api, options){
		    $(api.element).find('.buttons').hide();
		  },
		  enableTracking: true
		});
	</script>
</div>