<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php echo $title; ?></title>
<link rel="stylesheet" href="style/style.css">
<script type="text/javascript" src="script/jquery-1.11.0.min.js"></script>
</head>
<body>
	<a href="index.php">Home</a>
	<a href="index.php?controller=admin&action=index">Admin</a>
    <?php echo $content; ?>
  </body>
</html>