<?php
include_once dirname ( __FILE__ ) . '/../../lib/Controller.php';
class AdminController extends Controller {
	public function indexAction() {
		return $this->render ( 'admin/index.html.php', array (
				'title' => 'Admin' 
		) );
	}
}