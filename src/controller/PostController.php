<?php
include_once dirname ( __FILE__ ) . '/../../lib/Controller.php';
include_once dirname ( __FILE__ ) . '/../../lib/Driver.php';
include_once dirname ( __FILE__ ) . '/../entity/repository/PostRepository.php';
include_once dirname ( __FILE__ ) . '/../entity/repository/TagRepository.php';
include_once dirname ( __FILE__ ) . '/../entity/repository/CategoriaRepository.php';
include_once dirname ( __FILE__ ) . '/../entity/manager/PostManager.php';
class PostController extends Controller {
	public function newAction() {
		$tagRepository = new TagRepository ( Driver::getInstance () );
		$categoriaRepository = new CategoriaRepository ( Driver::getInstance () );
		
		$tags = $tagRepository->getAll ();
		$categorie = $categoriaRepository->getAll ();
		$post = new Post ();
		
		$this->render ( 'admin/post/form.html.php', array (
				'tags' => $tags,
				'categorie' => $categorie,
				'post' => $post 
		) );
	}
	public function listAction() {
		$postRepository = new PostRepository ( Driver::getInstance () );
		
		$posts = $postRepository->getAll ();
		
		$this->render ( 'admin/post/list.html.php', array (
				'posts' => $posts 
		) );
	}
	public function editAction() {
		$id = $_GET ['id'];
		
		$postRepository = new PostRepository ( Driver::getInstance () );
		$tagRepository = new TagRepository ( Driver::getInstance () );
		$categoriaRepository = new CategoriaRepository ( Driver::getInstance () );
		
		$post = $postRepository->getById ( $id );
		$tags = $tagRepository->getAll ();
		$categorie = $categoriaRepository->getAll ();
		
		$this->render ( 'admin/post/form.html.php', array (
				'post' => $post,
				'tags' => $tags,
				'categorie' => $categorie 
		) );
	}
	public function postAction() {
		// TODO: Dependency container
		$postRepository = new PostRepository ( Driver::getInstance () );
		$tagRepository = new TagRepository ( Driver::getInstance () );
		$categoriaRepository = new CategoriaRepository ( Driver::getInstance () );
		
		$tags = $tagRepository->getByIds ( $_POST ['tags_ids'] );
		$categoria = $categoriaRepository->getById ( $_POST ['categoria_id'] );
		
		if (isset ( $_POST ['id'] ) && ! empty ( $_POST ['id'] )) {
			$post = $postRepository->getById ( $_POST ['id'] );
		} else {
			$post = new Post ();
		}
		
		$post->setTitolo ( $_POST ['titolo'] );
		$post->setCorpo ( $_POST ['corpo'] );
		$post->setTags ( $tags );
		$post->setCategoria ( $categoria );
		
		$manager = new PostManager ( Driver::getInstance ()->getDBAL () );
		$manager->persist ( $post );
		
		header ( 'Location: index.php?controller=post&action=list' );
	}
	public function showAction() {
		$postRepository = new PostRepository ( Driver::getInstance () );
		
		$post = $postRepository->getById ( $_GET ['id'] );
		
		$this->render ( 'post/show.html.php', array (
				'title' => 'Post: ' . $post->getTitolo (),
				'post' => $post 
		) );
	}
}