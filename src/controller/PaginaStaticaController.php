<?php
include_once dirname ( __FILE__ ) . '/../../lib/Controller.php';
include_once dirname ( __FILE__ ) . '/../../lib/Driver.php';
include_once dirname ( __FILE__ ) . '/../entity/repository/PaginaStaticaRepository.php';
include_once dirname ( __FILE__ ) . '/../entity/manager/PaginaStaticaManager.php';
class PaginaStaticaController extends Controller {
	public function listAction() {
		$paginaStaticaRepository = new PaginaStaticaRepository ( Driver::getInstance () );
		
		$pagineStatiche = $paginaStaticaRepository->getAll ();
		
		$this->render ( 'admin/pagina_statica/list.html.php', array (
				'pagine_statiche' => $pagineStatiche 
		) );
	}
	public function newAction() {
		$paginaStatica = new PaginaStatica ();
		
		$this->render ( 'admin/pagina_statica/form.html.php', array (
				'title' => 'Nuova Pagina Statica',
				'pagina_statica' => $paginaStatica 
		) );
	}
	public function editAction() {
		$paginaStaticaRepository = new PaginaStaticaRepository ( Driver::getInstance () );
		
		$paginaStatica = $paginaStaticaRepository->getById ( $_GET ['id'] );
		
		$this->render ( 'admin/pagina_statica/form.html.php', array (
				'title' => 'Modifica Pagina Statica',
				'pagina_statica' => $paginaStatica 
		) );
	}
	public function postAction() {
		$paginaStaticaRepository = new PaginaStaticaRepository ( Driver::getInstance () );
		
		if (isset ( $_POST ['id'] ) && ! empty ( $_POST ['id'] )) {
			$paginaStatica = $paginaStaticaRepository->getById ( $_POST ['id'] );
		} else {
			$paginaStatica = new PaginaStatica ();
		}
		
		$paginaStatica->setTitolo ( $_POST ['titolo'] );
		$paginaStatica->setCorpo ( $_POST ['corpo'] );
		
		$manager = new PaginaStaticaManager ( Driver::getInstance ()->getDBAL () );
		$manager->persist ( $paginaStatica );
		
		header ( 'Location: index.php?controller=pagina_statica&action=list' );
	}
	public function showAction() {
		$paginaStaticaRepository = new PaginaStaticaRepository ( Driver::getInstance () );
		
		$paginaStatica = $paginaStaticaRepository->getById ( $_GET ['id'] );
		
		$this->render ( 'pagina_statica/show.html.php', array (
				'title' => 'Pagina Statica: ' . $paginaStatica->getTitolo (),
				'pagina_statica' => $paginaStatica 
		) );
	}
}