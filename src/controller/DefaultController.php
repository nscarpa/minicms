<?php
include_once dirname ( __FILE__ ) . '/../../lib/Controller.php';
include_once dirname ( __FILE__ ) . '/../../lib/Driver.php';
include_once dirname ( __FILE__ ) . '/../entity/repository/PostRepository.php';
include_once dirname ( __FILE__ ) . '/../entity/repository/PaginaStaticaRepository.php';
include_once dirname ( __FILE__ ) . '/../entity/repository/TagRepository.php';
class DefaultController extends Controller {
	public function indexAction() {
		$postRepository = new PostRepository ( Driver::getInstance () );
		$paginaStaticaRepository = new PaginaStaticaRepository ( Driver::getInstance () );
		
		$posts = $postRepository->getAll ();
		$pagineStatiche = $paginaStaticaRepository->getAll ();
		
		$this->render ( 'index.html.php', array (
				'title' => 'Blog',
				'posts' => $posts,
				'pagine_statiche' => $pagineStatiche 
		) );
	}
}